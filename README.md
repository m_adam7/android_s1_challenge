# Buseet - Coding task #


### Idea ###
The goal of this app is to gather the list of Foursquare food venues that are
close to the current location of the device and display them in a list. Every item of the list should
be clickable and upon being clicked the selected place should be focused and centered on the
map.

### Functional Requirements ###
The app has one screen, with two basic features. This screen features a full-screen map,
however, the screen is conceivably separated into two “panels".

#### 1 - The top panel “Map”: ####

- The current location of the user is obtained via the device's GPS, only once on app start.
- The current location should be a pin placed in the center of the map, non draggable pin. 
- The map is touchable and movable
- Every time the map settles in a new position, a Foursquare search request that queries the food restaurants of the visible rectangle of the map on the "top panel" should be executed, and the results should be projected on a list over the map, the bottom "list panel".


#### 2 - The bottom panel “List Panel”: ####

- List of the nearby Foursquare food places overlaying the map.
- The list of the nearby places should be ordered by distance, starting from the nearest place.
- Every time that an item is clicked, it should be displayed in an overlay view, according to [Abstract design](https://drive.google.com/file/d/1UjbnazyMo6f4rECCnM1XAzxuLkj9t7eM/view?usp=sharing).
- A red pin that points the location of the selected restaurant should be dropped in the map while the map "panel" centerd in the position of the venue clicked.
- Pressing the Android back button, the map should return to the first state and the overlay view of the selected restaurant should disappear and the map is centered again according to the current location marker.



### Implementation ###

- The nearby search API documentation can be found here: [Foursquare Search Venues API](https://developer.foursquare.com/docs/api-reference/venues/search/) (please query Foursquare for the food restaurant stores).
- The venues that are fetched as a result are displayed as items of the Foursquare list.
- Also please visit [Foursquare Developers Registration](https://foursquare.com/developers/login?continue=/developers/register) to obtain a developer’s account.
- UnitTests is needed.

*Feel free to use any third-party library that helps you complete this task.

*Finally, don’t forget to display any error and loading dialogs when necessary.

### Bonus points ###

- Integrate CI/CD tool to the project.

### ATTENTION! ###
Please don't share this document neither with anybody nor to any public website.

### Submit your challenge ###
- Fork the Challenge Repository
- Create a new branch and push your commits to it as you would do in a real-world task
- Issue a Pull Request

*NOTE: This challenge is aimed to senior developers, so expected to hit a high point of technicality.*

Good luck!